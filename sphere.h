#ifndef SPHERE_H
#define SPHERE_H
#include "graphics.h"
#include "location.h"
#include "enumsshape.h"  //enum declaration of shapes

 class matter
  {
     private:
     vertex *m_Ptr_Vertex;  //pointer to hold sphere vertex values.

     int m_nTexture; //holds identification number of assigned texture

     matter(); //default matter constructor

     public:
loc pos;
     int nVertexCount; //Vertex count

     matter(shapez choice,int nSize,int nPcount,double x,double y,double z);  //constructor

     int cSphere(int nSize,int nPcount); //spherical cordinate assign0r

     ~matter();
 };
 #endif
