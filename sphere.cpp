#include "sphere.h"
#include "math.h"
#include <iostream>

const double PI=3.1415926535897;
matter::matter(shapez choice,int nSize,int nPcount,double x,double y,double z):pos(x,y,z) //no matter the matter location class is initialized..
     {

switch(choice)
{
case 1:
cSphere(nSize,nPcount);
break;
}

     }

int matter::cSphere(int nSize, int nPcount)   //vertex assignat0r like a terminator
     {
         nVertexCount=(360/nPcount)*(90/nPcount)*4;
         m_Ptr_Vertex=new vertex[nVertexCount];
         int n=0;
         // dynamically create graphics array to hold sphere coordinates
         for(double a=0;a<90-nPcount;a+=nPcount) //altitude angle counter
         {
          for(double b=0;b<360-nPcount;b+=nPcount)  //azimuth angle

          {
                 m_Ptr_Vertex[n].setvalue('x',nSize*(sin((a) / 180 * PI) * sin((b) / 180 * PI)));
                 m_Ptr_Vertex[n].setvalue('y',nSize*(cos((a) / 180 * PI) * sin((b) / 180 * PI)));
                 m_Ptr_Vertex[n].setvalue('z',nSize*(cos((b) / 180 * PI)));

                 n++;

                 m_Ptr_Vertex[n].setvalue('x',nSize*(sin((a) / 180 * PI) * sin((b+nPcount) / 180 * PI)));
                 m_Ptr_Vertex[n].setvalue('y',nSize*(cos((a) / 180 * PI) * sin((b+nPcount) / 180 * PI)));
                 m_Ptr_Vertex[n].setvalue('z',nSize*(cos((b+nPcount) / 180 * PI)));
                 n++;

                 m_Ptr_Vertex[n].setvalue('x',nSize*(sin((a+nPcount) / 180 * PI) * sin((b) / 180 * PI)));
                 m_Ptr_Vertex[n].setvalue('y',nSize*(cos((a+nPcount) / 180 * PI) * sin((b) / 180 * PI)));
                 m_Ptr_Vertex[n].setvalue('z',nSize*(cos((b) / 180 * PI)));
                 n++;
                 m_Ptr_Vertex[n].setvalue('x',nSize*(sin((a+nPcount) / 180 * PI) * sin((b+nPcount) / 180 * PI)));
                 m_Ptr_Vertex[n].setvalue('y',nSize*(cos((a+nPcount) / 180 * PI) * sin((b+nPcount) / 180 * PI)));
                 m_Ptr_Vertex[n].setvalue('z',nSize*(cos((b+nPcount) / 180 * PI)));
                 n++;


          }
         }

     }

matter::~matter()   //destructor
{
    delete[]m_Ptr_Vertex;   //delete dnamically assigned array
    m_Ptr_Vertex=0;   // null terminate the pointer
}
