int loadtexture(const char *filename, float width, float height)
{
    using namespace std;
    int texture;
    int nTexsize=width*height*3;
    char *data=new char[nTexsize];
    iffstream iff(filename);
    iff.read(data,height*width*3);
    glGenTextures(1,&texture);
    glBindTexture(GL_TEXTURE_2D,texture);
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
    glTexParameterf(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
    gluBuild2DMipmaps(GL_TEXTURE_2D,3,width,height,GL_RGB,GL_UNSIGNED_BYTE,data);
    data = 0;
    delete[] data;
    return texture;
}
