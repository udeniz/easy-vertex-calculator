#include <fstream>
#include "GL\gl.h"
#include "GL\glut.h"
int loadtextures (const char *filename, float width, float height)  //texture loading function
{
    using namespace std;
    GLuint texture;   //texture is a number to identify our texture in array
    int nsize=width*height*3;  // 3 times the area of our texture is allocated as memory amount
    char *data= new char[nsize]; //dynamically array size allocated
    ifstream iff(filename);  //define iff as the input file stream class
    iff.read(data,height*width*3);// read into pre defined *data array

  glGenTextures( 1, &texture ); // bind our texture as the first one
  glBindTexture( GL_TEXTURE_2D, texture );  //bind our texture
  glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );
  glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
  gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,GL_RGB, GL_UNSIGNED_BYTE, data );

  data = NULL;

  return texture;
}
