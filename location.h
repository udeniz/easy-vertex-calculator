#ifndef LOC_H
#define LOC_H
class loc
{
    private:
    loc();

    double Xpos;
    double Ypos;
    double Zpos;

    public:
    loc(double x, double y, double z);
    double setvalue(const char value,double dNewValue);
    double getvalue(const char value);
};
#endif
